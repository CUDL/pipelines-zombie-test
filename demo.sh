#!/bin/sh
PS_COLUMNS=pid,ppid,stat,args

(sleep 1 & echo ${!} > /tmp/zombie.pid; wait) &
subshell_pid=${!}
sleep 0.5
zombie_pid="$(cat /tmp/zombie.pid)"

echo "sleep process (which may become stuck as a zombie) PID: $zombie_pid"
echo
echo "Processes before killing subshell:"
ps -eo $PS_COLUMNS


echo
echo "Processes after killing subshell:"
kill $subshell_pid
ps -eo $PS_COLUMNS

for i in 1 2 3 4 5; do
  sleep 1
  echo
  echo "Processes $i second(s) after sleep process has terminated:"
  ps -eo $PS_COLUMNS
done

if [ "$(ps -q $zombie_pid -o stat --no-headers)" = "Z" ]; then
    echo "Error: sleep process adopted by PID 1 has not been reaped" 1>&2
    exit 1
fi

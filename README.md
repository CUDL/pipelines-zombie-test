# Bitbucket Pipelines process reaping issue demo

This repo contains a pipelines build which demonstrates orphaned child
processes not being reaped (remaining zombies) when run in the pipelines
environment.

This is a common problem with container environments, see: https://blog.phusion.nl/2015/01/20/docker-and-the-pid-1-zombie-reaping-problem/
